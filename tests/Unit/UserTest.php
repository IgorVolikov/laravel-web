<?php

namespace Tests\Unit;

use App\Models\ResetPassword;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $data = ['name' => 'Igor10', 'email' => 'Igor10@gmail.com', 'password' => bcrypt('123456781')];

        $userService = new UserService();
        $createdUser = $userService->createUser($data);
        unset($data['password']);

        $this->assertInstanceOf(User::class, $createdUser);
        $this->assertDatabaseHas('users', $data);
    }

    public function testSendEmailResetPassword()
    {
        $data = ['user_id' => 94, 'email' => 'Igor10@gmail.com', 'token' => Str::random(64)];

        $userService = new UserService();
        $passwordReset = $userService->createPasswordReset($data);
        unset($data['email']);

        $this->assertInstanceOf(ResetPassword::class, $passwordReset);
        $this->assertDatabaseHas('password_resets', $data);
    }

    public function testUpdateUser()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();

        $dataUpdate = ['name' => 'Igor2', 'email' => 'Igor2@gmail.com'];
        $userService = new UserService();
        $userService->updateUser($dataUpdate, $user);

        $this->assertDatabaseHas('users', $dataUpdate);
    }

    public function testGetUsers()
    {
        $this->artisan('passport:install');
        User::factory()->create();

        $userService = new UserService();
        $this->assertIsArray($userService->getUsers());
    }

    public function testGetUser()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();

        $userService = new UserService();
        $this->assertIsObject($userService->getUser($user->id));
    }

    public function testDeleteUser()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();

        $userService = new UserService();
        $this->assertIsObject($userService->deleteUser($user->id));
        $this->assertDatabaseHas('users', ['status' => 2]);
    }
}
