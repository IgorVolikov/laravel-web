<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\User;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $this->artisan('passport:install');

        $data = ['name' => 'Igor11','email' => 'Igor11@gmail.com', 'password' => '123456781', 'password_c' => '123456781'];

        $response = $this->json('POST', 'api/users', $data)->assertStatus(201);
        $response->assertJsonStructure(['token', ]);
        $this->assertDatabaseHas('users', ['email' => 'Igor11@gmail.com']);
    }

    public function testLoginUser()
    {
        $this->artisan('passport:install');

        $data = ['name' => 'Igor11', 'email' => 'Igor11@gmail.com', 'password' => bcrypt('123456781')];
        User::factory()->create($data);

        //login: positive result
        unset($data['name']);
        $data['password'] = '123456781';
        $response = $this->json('POST', 'api/login', $data)->assertStatus(200);
        $response->assertJsonStructure(['token', ]);

        //login: negative result
        $dataLoginNegative = ['email' => 'Igor12@gmail.com', 'password' => '123456781'];
        $this->json('POST', 'api/login', $dataLoginNegative)->assertStatus(401);
    }

    public function testSendEmailResetPassword()
    {
        $this->artisan('passport:install');

        $data = ['name' => 'Igor11', 'email' => 'Igor11@gmail.com', 'password' => bcrypt('123456781')];
        User::factory()->create($data);

        unset($data['name']);
        unset($data['password']);
        Mail::fake();
        $this->json('POST', 'api/sendEmailResetPassword', $data);
        Mail::assertSent(ResetPasswordMail::class);
    }

    public function testResetUserPassword()
    {
        $password = bcrypt('123456781');
        $token = Str::random(64);

        $response = $this->json('POST', 'api/resetUserPassword', ['token' => $token, 'password' => $password]);
        $response->assertJsonStructure();
    }

    public function testUpdateUser()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();
        $dataUpdate = ['name' => 'Igor8', 'email' => 'Igor8@gmail.com'];
        Passport::actingAs($user);

        $this->putJson("api/users/{$user->id}", $dataUpdate)->assertStatus(200);
        $this->assertDatabaseHas('users', $dataUpdate);
    }

    public function testGetUsers()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->json('GET', 'api/users')->assertStatus(200);
        $response-> assertJsonStructure(['users', ]);
    }

    public function testGetUser()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();
        Passport::actingAs($user);

        $response = $this->json('GET', "api/users/{$user->id}")->assertStatus(200);
        $response->assertJsonFragment(['created_at' => $user->created_at, 'email' => $user->email, 'id' => $user->id, 'name' => $user->name, 'updated_at' => $user->updated_at]);
    }

    public function testDeleteUser()
    {
        $this->artisan('passport:install');
        $user = User::factory()->create();
        Passport::actingAs($user);

        $this->deleteJson("api/users/{$user->id}")->assertStatus(200);
        $this->assertDatabaseHas('users', ['status' => 2]);
    }
}
