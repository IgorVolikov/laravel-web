<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\PassportAuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('users', [PassportAuthController::class, 'store']);
Route::post('login', [PassportAuthController::class, 'login'])->name('login');
Route::post('sendEmailResetPassword', [PassportAuthController::class, 'sendEmailResetPassword']);
Route::post('resetUserPassword', [PassportAuthController::class, 'resetUserPassword']);

Route::middleware('auth:api')->group(function () {
    Route::put('users/{user}', [PassportAuthController::class, 'updateUser']);
    Route::get('users', [PassportAuthController::class, 'index']);
    Route::get('users/{id}', [PassportAuthController::class, 'show']);
    Route::delete('users/{id}', [PassportAuthController::class, 'delete']);
});
