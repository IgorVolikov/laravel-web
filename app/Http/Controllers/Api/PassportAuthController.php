<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeleteUserDeleteRequest;
use App\Models\User;
use App\Services\UserService;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\LoginPostRequest;
use App\Http\Requests\SendEmailResetPasswordPostRequest;
use App\Http\Requests\ResetPasswordPostRequest;
use App\Http\Requests\UpdateUserPutRequest;
use App\Http\Requests\GetUserGetRequest;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class PassportAuthController extends Controller
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Registration Req
     */
    public function store(StorePostRequest $request)
    {
        $user = $this->userService->createUser($request->validated());
        $token = $user->createToken('LaravelPassportToken')->accessToken;

        return response()->json(['token' => $token], 201);
    }

    /**
     * Login Req
     */
    public function login(LoginPostRequest $request)
    {
        if (auth()->attempt($request->validated())) {
            $token = auth()->user()->createToken('LaravelPassportToken')->accessToken;
            return response()->json(['token' => $token]);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    /**
     * sendEmailResetPassword Req
     */
    public function sendEmailResetPassword(SendEmailResetPasswordPostRequest $request)
    {
        $user = DB::table('users')->where('email', '=', $request->email)->first();

        $token = Str::random(64);
        $data = ['user_id' => $user->id, 'token' => $token, 'email' => $request->email];
        $this->userService->createPasswordReset($data);

        return response()->json(['token' => $token]);
    }

    /**
     * sendEmailResetPassword Req
     */
    public function resetUserPassword(ResetPasswordPostRequest $request)
    {
        $token = $request->token;
        $existedToken = DB::table('password_resets')->where('token', '=', $token)->first();

        $user_id = $existedToken->user_id;
        $newPassword = $request->password;

        //check if token was created less than 2 hours ago
        $dateOfTokenCreation = Carbon::parse($existedToken->created_at);
        $diff = $dateOfTokenCreation->diffInHours(Carbon::now());
        if($diff > 2) {
            return response()->json(['error' => 'token has expired'], 422);
        }

        //update user password and updated_at fields
        DB::table('users')->where("id", $user_id)->update(["password" => bcrypt($newPassword), "updated_at" => now()]);

        //delete token from the database
        DB::table('password_resets')->where('token', $token)->delete();

        return response()->json(null);
    }

    public function updateUser(UpdateUserPutRequest $request, User $user)
    {
        $updatedUser = $this->userService->updateUser($request->validated(), $user);

        return response()->json($updatedUser);
    }

    public function index()
    {
        $users = $this->userService->getUsers();

        return response()->json(['users' => $users]);
    }

    public function show(GetUserGetRequest $request, $id)
    {
        return response()->json($this->userService->getUser($id));
    }

    public function delete(DeleteUserDeleteRequest $request, $id)
    {
        return response()->json($this->userService->deleteUser($id));
    }
}
