<?php

namespace App\Services;

use App\Mail\ResetPasswordMail;
use App\Models\User;
use App\Models\ResetPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Resources\UserResource;
use PDF;

class UserService
{
    public function createUser(array $data):User
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
    }

    public function createPasswordReset(array $data):ResetPassword
    {
        Mail::to($data['email'])->send(new ResetPasswordMail($data['token']));

        return ResetPassword::create([
            'user_id' => $data['user_id'],
            'token' => $data['token']
        ]);
    }

    public function updateUser(array $data, $user):User
    {
        if(array_key_exists('name', $data)) {
            $user->name = $data['name'];
        }

        if(array_key_exists('email', $data)) {
            $user->email = $data['email'];
        }

        $user->save();
        return $user;
    }

    public function getUsers()
    {
        return User::all()->pluck('email')->toArray();
    }

    public function getUser($id):UserResource
    {
        return new UserResource(User::find($id));
    }

    public function deleteUser($id):UserResource
    {
        $user = User::find($id);
        DB::table('users')->where("id", $id)->update(["status" => User::INACTIVE]);

        $data["title"] = "From unknown.com";
        $data["body"] = "Dear $user->name! Your account has been unactivated!";

        $pdf = PDF::loadView('emails.DeleteUserMail', $data);

        Mail::send('emails.DeleteUserMail', $data, function($message)use($data, $pdf, $user) {
            $message->to($user->email, $user->email)
            ->subject($data["title"])
            ->attachData($pdf->output(), "AccountInformation.pdf");
        });

        return new UserResource($user);
    }
}
