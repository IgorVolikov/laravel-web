<x-mail::message>
# Introduction
Your token: {{ $token }}

To reset your password please push the button:

<x-mail::button :url="''">
Reset password
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
